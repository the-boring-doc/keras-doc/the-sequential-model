# The sequential model

This is the source code for the keras sequential model.
## Requirements
- [python](https://www.python.org/)
- [anaconda](https://www.anaconda.com/)
- [keras](https://pypi.org/project/face-recognition/)
- [tensorflow](https://pypi.org/project/opencv-python/)

## Setup
- Clone the project
```shell script
$> git clone https://gitlab.com/the-boring-doc/keras-doc/the-sequential-model.git
```
- Just open the file 'The Sequential model.ipynb' with notebook jupyter 
- Don't forget to set the right envirenoment with keras installed
- To install kears, tensorflow go to anaconda env and click in play button -> open terminal 
and in the shell 
```shell script
$> conda install -c conda-forge keras
```
```shell script
$> conda install -c conda-forge tensorflow
```

TADAAA !!!
